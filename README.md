# Floor plans Semantic Segmentation

## Code:

[Training loop](EDA.ipynb)
[Evaluation](Results_Demo.ipynb)
[Dataloader and augmentation](new_resloader.py) works not only for walls "w.png", but for doors, windows, balconies and entrances too.

## The Task:

Train model on extra small dataset of 16 images

### Input image
![Input image](o.png "Input image")

### Desired output
![Desired mask output](w.png "Desired mask output")

## Results:

IoU 79% achieved.

![Results](result.png "Results")
