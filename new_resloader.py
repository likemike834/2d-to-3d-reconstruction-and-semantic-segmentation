import os
from os.path import exists
import torch
import torchvision
from PIL import Image, ImageOps
import cv2
from torchvision import transforms
import sys
import numpy as np
import torchvision.transforms.functional as TF
import random


class MyRotationTransform:
    """Rotate by one of the given angles."""

    def __init__(self, angles):
        self.angles = angles

    def __call__(self, x):
        angle = random.choice(self.angles)
        return TF.rotate(x, angle)


torch.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(threshold=sys.maxsize)

class Dataset(torch.utils.data.Dataset):
    def __init__(self, inputs, batch_size, masks):
        self.inputs = inputs
        self.length = batch_size
        self.masks = masks

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        # Select sample
        inputs_size = len(self.inputs)
        if index > inputs_size - 1:
            dir = self.inputs[torch.randint(0, inputs_size, (1,))]
        else:
            dir = self.inputs[index]
        origin = cv2.imread(os.path.join(dir, 'o.png'))
        stack = cv2.cvtColor(origin, cv2.COLOR_BGR2RGB)
        for file in self.masks:
            file_path = os.path.join(dir, file+'.png')
            if exists(file_path):
                mask = cv2.imread(file_path)
                if file == 'bg':
                    balcony_path = os.path.join(dir, 'b.png')
                    if exists(balcony_path):
                        balcony_mask = cv2.imread(balcony_path)
                        mask = mask + balcony_mask
                    room_path = os.path.join(dir, 'r.png')
                    room_mask = cv2.imread(room_path)
                    mask = mask + room_mask
                mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
                mask = mask[:, :, None]
            else:
                mask = torch.zeros((origin.shape[0], origin.shape[1], 1))
            stack = np.concatenate((stack, mask), 2)
        tr = transforms.Compose([
            transforms.ToTensor(),
            transforms.RandomCrop(size=(512, 512)),
            transforms.RandomHorizontalFlip(p=0.5),
            transforms.RandomApply(transforms=[
                MyRotationTransform(angles=[-90, 90, 180])
            ], p=0.5),
        ])
        stack = tr(stack)
        x = stack[0:3, :, :]
        y = stack[3:, :, :]
        trx = transforms.Compose([
            transforms.RandomApply(transforms=[
                transforms.GaussianBlur(kernel_size=(5, 9), sigma=(0.1, 5)),
                transforms.RandomAdjustSharpness(sharpness_factor=2, p=0.5),
            ], p=0.5),
            transforms.RandomApply(transforms=[
                transforms.RandomAutocontrast(p=0.5),
                transforms.RandomInvert(p=0.5),
            ], p=0.5)
        ])
        x = trx(x)
        normalizer = transforms.Normalize((0.7128, 0.7113, 0.7055), (0.3947, 0.3904, 0.3886))
        x = normalizer(x)
        return x, y
